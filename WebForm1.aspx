<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="cal2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #DisplayText {
            width: 108px;
        }
        #Text1 {
            width: 18px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="Button1" runat="server" Text="1" OnClick="Button_Click" Height="25px" Width="26px" />
            <asp:Button ID="Button2" runat="server" Text="2" OnClick="Button_Click" Width="26px" />
            <asp:Button ID="Button3" runat="server" Text="3" OnClick="Button_Click" Width="25px" />
            <asp:Button ID="ButtonAdd" runat="server" Text="+" OnClick="ButtonOperator_Click" Width="25px" />
            <asp:Button ID="ButtonDiv" runat="server" Text="/" OnClick="ButtonOperator_Click" Width="25px" />
        </div>
        <div>
            <asp:Button ID="Button4" runat="server" Text="4" OnClick="Button_Click" Width="26px" />
            <asp:Button ID="Button5" runat="server" Text="5" OnClick="Button_Click" Width="26px" />
            <asp:Button ID="Button6" runat="server" Text="6" OnClick="Button_Click" Width="25px" />
            <asp:Button ID="ButtonMinus" runat="server" Text="-" OnClick="ButtonOperator_Click" Width="25px" />
            <asp:Button ID="ButtonMul" runat="server" Text="*" OnClick="ButtonOperator_Click" Width="25px" />           
        </div>
        <div>
            <asp:Button ID="Button7" runat="server" Text="7" OnClick="Button_Click" Width="26px" />
            <asp:Button ID="Button8" runat="server" Text="8" OnClick="Button_Click" Width="26px" />
            <asp:Button ID="Button9" runat="server" Text="9" OnClick="Button_Click" Width="25px" />
            <asp:Button ID="ButtonDel" runat="server" Text="Del" Width="25px" OnClick="ButtonDel_Click" />
            <asp:Button ID="ButtonC" runat="server" Text="C" Width="25px" OnClick="Clear_Click" />
        </div>       
        <div>
            <asp:Button ID="Button" runat="server" Text="0" Width="26px" OnClick="Button_Click" />
            <asp:Button ID="ButtonDec" runat="server" Text="." Width="26px" OnClick="Button_Click" />
            <asp:Button ID="ButtonEnter" runat="server" Text="Enter" OnClick="ButtonEnter_Click" Width="75px" />
        </div>       
        <div>
            
            <asp:TextBox ID="Display" runat="server" OnTextChanged="Display_TextChanged">0</asp:TextBox>
            
        </div>
    </form>
</body>
</html>
