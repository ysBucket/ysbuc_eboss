using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cal2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public String number;
        public double value;
        public String operatorString;
        public bool operatorPressed = false;
        public bool permitOperator = true;
        public double Result;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.ButtonEnter.Click += new System.EventHandler(this.ButtonEnter_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Button_Click(object sender, EventArgs e)
        {
            operatorPressed = Convert.ToBoolean(Session["Operator"]);
            permitOperator = Convert.ToBoolean(Session["PermitOperator"]);
            permitOperator = true;
            Session["PermitOperator"] = permitOperator;

            //Check if the textbox is zero or operator has been pressed
            if ((Display.Text == "0") || (operatorPressed))
            {
                Display.Text = string.Empty;
                
    }
            operatorPressed = false;
            Session["Operator"] = operatorPressed;

            if ((sender as Button).Text == ".")
            {
                if (!Display.Text.Contains("."))
                {
                    Display.Text = Display.Text + (sender as Button).Text;
                }
            }
            else
            {
                Display.Text = Display.Text + (sender as Button).Text;
            }
        }

        protected void ButtonOperator_Click(object sender, EventArgs e)
        { 
            if (Convert.ToDouble(Session["Value"]) != 0)
            {
                operatorPressed = true;
                Session["Operator"] = operatorPressed;

                permitOperator = Convert.ToBoolean(Session["PermitOperator"]);

                if (permitOperator == true)
                {
                    ButtonEnter_Click(null, null);                    
                }

                permitOperator = false;
                Session["PermitOperator"] = permitOperator;

                operatorString = (sender as Button).Text;
                Session["OperatorString"] = operatorString;
            }
            else {
                value = Convert.ToDouble(Display.Text);
                Session["Value"] = value;

                operatorString = (sender as Button).Text;
                Session["OperatorString"] = operatorString;

                //Operator button has been clicked,  
                operatorPressed = true;
                Session["Operator"] = operatorPressed;
            }
            
        }

        protected void ButtonEnter_Click(object sender, EventArgs e)
        {
            value = Convert.ToDouble(Session["Value"]);
            Response.Write(value);

            operatorString = Convert.ToString(Session["OperatorString"]);
            Response.Write(operatorString);

            switch (operatorString)
            {
                case "+":
                    Result = value + Convert.ToDouble(Display.Text);
                    break;
                case "-":
                    Result = value - Convert.ToDouble(Display.Text);
                    break;
                case "*":
                    Result = value * Convert.ToDouble(Display.Text);
                    break;
                case "/":
                    Result = value / Convert.ToDouble(Display.Text);
                    break;
                default:
                    break;
            }
            Display.Text = Result.ToString();
            value = Convert.ToDouble(Display.Text);
            Session["Value"] = value;
        }

        protected void ButtonDel_Click(object sender, EventArgs e)
        {
            number = Display.Text;

            if (number.Length >= 1 && number != "0")
            {
                Display.Text = number.Substring(0, number.Length - 1);
            }
        }

        protected void Clear_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Display.Text = "0";
        }

        protected void Display_TextChanged(object sender, EventArgs e)
        {

        }
    }
}